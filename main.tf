provider "azurerm" {
    version = "~>1.40"

    subscription_id = "50505e05-cd97-4b8d-a080-40b5b880ce44"
    tenant_id = "34f55fb5-3a46-4626-b706-5d94d061b0d4"
    #client_id = "xxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx"
    #client_secret = "xxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx"
}

resource "azurerm_resource_group" "rg" {
    name = "QuickstartTerraformTest-rg"
    location = "eastus"
}

# Storage account example

resource "azurerm_storage_account" "example" {
  name                     = "storageaccountexampletf"
  resource_group_name      = azurerm_resource_group.rg.name
  location                 = azurerm_resource_group.rg.location
  account_tier             = "Standard"
  account_replication_type = "GRS"

  tags = {
    environment = "staging"
  }
}